'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  AsyncStorage
} from 'react-native';

import { Container,
		     Content, 
         Header,
         Text,
         Button,
         Icon,
         Title,
         Left,
         Thumbnail,
         Body,
         Card,
         CardItem,
         Right
       } from 'native-base';

import Loader from '../Indicator/loader';

class pasien extends Component {

   static navigationOptions = ({ navigation }) => {
  const {state} = navigation;
  console.log(state.params);
  if (state.params != undefined ) {
  return {
    title: state.params.status,
  };
  }
};

   constructor() {
    super();
    this.state = {
      data : [],
      status: '',
      judul: '',
      loading : true,
      avatarSource: null,
    }
  }

    componentDidMount() {
        const { navigate } = this.props.navigation;
        AsyncStorage.getItem('statusUser', (error, results) => {
            if (results == 'dokter') {
            }else{
            AsyncStorage.getItem('idUser', (error, result) => {
                    if (result) {
                      navigate('Input', {id : result})
                    }
                  });
            }
          });
        this.getAllData();
        AsyncStorage.getItem('statusUser', (error, result) => {
            if (result) {
                  this.setState({
                  loading: false,
                });
                this.setState({
                  status: result,
                });
            }
          });
    }


    getAllData(){
        AsyncStorage.getItem('tokenUser', (error, result1) => {
            if (result1) {
                      AsyncStorage.getItem('idUser', (error, result) => {
                        if (result) {
                              fetch('http://dev.infinite-creative.com/sispak_api/Pasien/dokter/' + result, {
                               method: 'GET',
                               headers: {
                                            'Content-Type': 'application/json',
                                            'Accept': 'application/json',
                                            'token': result1
                                          }
                                })
                                .then((response) => response.json())
                                .then((response) => {

                                  this.setState({
                                      data : response.data,
                                    });

                                   this.setState({
                                      loading : false,
                                    });
                      
                                }).done();
                        }
                    });
            }
        });
  
    }

    _renderPasien () {
       const { navigate } = this.props.navigation;
          console.log(this.state.data);
            let dataPasien = this.state.data.map((data, index) => {
              let source;
              var url_photo = "http://dev.infinite-creative.com/sispak_api/upload/";

              source = {uri: url_photo+data.foto, isStatic: true};
                    return (
                            <Card key={index} style={styles.st_card1}>
                                <CardItem>
                                    <Left>
                                      <Thumbnail source={source} />
                                      <Body>
                                        <Text style={styles.right}>{data.nama}</Text>
                                        <Text note style={styles.right}>{data.tanggal_lahir}</Text>
                                      </Body>
                                    </Left>
                                     <Right >
                                      <Icon name="arrow-forward" onPress={() => navigate('Input', {id : data.id_pasien})}/>
                                    </Right>
                                </CardItem>
                              </Card>
                      )  
                });

          return (
             
                  <Content>
                      <Button transparent style={styles.st_header} onPress={() => navigate('DrawerOpen')}>
                        <Icon ios='ios-menu' android="md-menu" style={styles.st_size} />
                      </Button>
                        <Title style={styles.st_title1}> PASIEN </Title>
                           {dataPasien}
                    </Content>
            );
    }


  render() {
     const { navigate } = this.props.navigation;
  
     if (this.state.data.length > 0) {
                return (
                  <Container style={styles.container}>
                   <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                    <Loader loading={this.state.loading} />
                      {this._renderPasien()}
                  </Container>
                );
          }else{
            return (
                  <Container style={styles.container}>
                       <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                      <Content>
                        <Loader loading={this.state.loading} />
                       <Button transparent style={styles.st_header} onPress={() => navigate('DrawerOpen')}>
                        <Icon ios='ios-menu' android="md-menu" style={styles.st_size} />
                      </Button>
                          <Title style={styles.st_title1}> PASIEN </Title>
                      </Content>
                  </Container>
            );
          }
        
  
  }
}

const styles = StyleSheet.create({
	container:{
      flex: 1,
//      backgroundColor: '#16a085',
    },
    bgimage:{
      flex: 1,
      resizeMode: 'cover',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    st_header:{
    	marginTop: 15,
    },
    st_size:{
    	fontSize: 30,
    	color: 'white',
    },
    st_title1:{
      alignItems: 'center',
      fontSize: 20,
      fontWeight: '400', 
      marginBottom: 20,
    },
    st_title3:{
      color: '#ffffff', 
      fontSize: 17,
      marginLeft: 40,
      marginRight: 40,
    },
    st_card1:{
      flex : 0,
      marginTop: 0,
      marginLeft: 15,
      marginRight: 15,
    },
    right : {
      width:100,
    }
});

export default pasien;