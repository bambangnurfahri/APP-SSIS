'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';

import { Container,
		     Content, 
         Header,
         Body,
         Text,
         Button,
         Icon,
         Title,
         Card,
         Left,
         CardItem,
         Right
       } from 'native-base';

import Loader from '../Indicator/loader';

class detailDataPasien extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data : [],
      loading : true,
      username : '',
    }
  }

    componentDidMount() {
        this.getAllData();
    }


    getAllData(){
        AsyncStorage.getItem('tokenUser', (error, result1) => {
            if (result1) {

                            const {state} = this.props.navigation;

                              fetch('http://dev.infinite-creative.com/sispak_api/Survey/detail/' + state.params.id_survey, {
                               method: 'GET',
                               headers: {
                                            'Content-Type': 'application/json',
                                            'Accept': 'application/json',
                                            'token': result1
                                          }
                                })
                                .then((response) => response.json())
                                .then((response) => {
                                  console.log(response);
                                  this.setState({ 
                                      data : response.data,
                                    });

                                  this.setState({ 
                                      loading : false,
                                    });

                                     AsyncStorage.getItem('Username', (error, result2) => {

                                      this.setState({ 
                                        username : result2,
                                      });

                                  });
                      
                                }).done();
            }
        });
  
    }

    _renderSurvey() {
       const { navigate } = this.props.navigation;
            let dataSurvey = this.state.data.map((data, index) => {
                    return (
                          <Card key={index} style={styles.st_card1}>
                              <CardItem>
                              <Left>
                                <Icon active name="medkit" />
                                <Body>
                                  <Text style={styles.right}>{data.nama_parameter}</Text>
                                  <Text note style={styles.right}>{data.nama_grading}</Text>
                                </Body>
                              </Left>
                            </CardItem>
                        </Card>
                      )  
                });

          return (
              <Content style={styles.st_header}>
                  <Title style={styles.st_title1}> Input Data </Title>
                  <Title style={styles.st_nama}> {this.state.username} </Title>
                  <View style={styles.footerBottomSignUp}>
                  <TouchableOpacity onPress={() => navigate('Beranda')}>
                    <Text style={styles.st_signup}>
                      Kembali ke beranda {'\n'}
                    </Text>
                  </TouchableOpacity>
                  </View>
                  {dataSurvey}
              </Content>
            );
    }


  render() {
     const { navigate } = this.props.navigation;
    if (this.state.data.length > 0 ) {
          return (
            <Container style={styles.container}>
            <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
              <Loader loading={this.state.loading} />
               {this._renderSurvey()}
            </Container>
          );
    }else{
        return (
            <Container style={styles.container}>
            <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
              <Loader loading={this.state.loading} />
                 <Content style={styles.st_header}>
                    <Title style={styles.st_title1}> Input Data </Title>
                    <Title style={styles.st_nama}> {this.state.username} </Title>
                    
                </Content>
            </Container>
        );
    }
  
  }
}

const styles = StyleSheet.create({
	container:{
	  flex: 1,
	},
	bgimage:{
      flex: 1,
      resizeMode: 'cover',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      width: '100%',
      height: '100%',
//      backgroundColor: '#16a085',
    },
    st_header:{
    	marginTop: 20,
    },
    st_nama :{
      color:'white',
      marginBottom:20,
    },
    st_size:{
    	fontSize: 30,
    	color: 'white',
    },
    st_title1:{
      color:'white',
      alignItems: 'center',
      fontSize: 20,
      fontWeight: '400',
      marginTop:20,
    },
    st_title3:{
      color: '#ffffff', 
      fontSize: 17,
      marginLeft: 40,
      marginRight: 40,
    },
    st_card1:{
      marginTop: 0,
      marginLeft: 15,
      marginRight: 15,
    },
    st_signup:{
      color: 'white',
      alignItems: 'center',
      fontWeight: '500', 
      marginLeft:120,
    },
    right : {
      width:200,
    }
});

export default detailDataPasien;