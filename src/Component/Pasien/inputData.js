'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';

import { Container,
		     Content, 
         Header,
         Body,
         Text,
         Button,
         Icon,
         Title,
         Card,
         Left,
         CardItem,
         Right
       } from 'native-base';

import Loader from '../Indicator/loader';

class inputData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data : [],
      status: '',
      username : '',
      nama :'',
      loading: true,
    }
  }

    componentDidMount() {
        this.getAllData();
    }


    getAllData(){
        AsyncStorage.getItem('tokenUser', (error, result1) => {
            if (result1) {

                            const {state} = this.props.navigation;

                              fetch('http://dev.infinite-creative.com/sispak_api/Survey/pasien/' + state.params.id, {
                               method: 'GET',
                               headers: {
                                            'Content-Type': 'application/json',
                                            'Accept': 'application/json',
                                            'token': result1
                                          }
                                })
                                .then((response) => response.json())
                                .then((response) => {
                                
                                  this.setState({ 
                                      data : response.data,
                                    });

                                   this.setState({
                                      loading : false,
                                    });

                                  AsyncStorage.getItem('statusUser', (error, results) => {

                                      this.setState({ 
                                        status : results,
                                      });

                                  });

                                    AsyncStorage.getItem('Username', (error, result2) => {

                                      this.setState({ 
                                        username : result2,
                                      });

                                     

                                  });

                                  AsyncStorage.getItem('nama', (error, result3) => {

                                    this.setState({ 
                                      nama : result3,
                                    });
                                  });
                      
                                }).done();
            }
        });
  
    }

    _renderSurvey() {
       const { navigate } = this.props.navigation;

       if (this.state.status == 'dokter') {
           let dataSurvey = this.state.data.map((data, index) => {
                    return (
                          <Card key={index} style={styles.st_card1}>
                              <CardItem>
                              <Left>
                                <Icon active name="medkit" />
                                <Body>
                                  <Text style={styles.right}>{data.tanggal}</Text>
                                  <Text style={styles.right}>{data.jam}</Text>
                                </Body>
                              </Left>
                              <Right>
                                <Icon name="arrow-forward" onPress={() => navigate('Detail', {id_survey : data.id_survey, valid : data.valid})}/>
                              </Right>
                            </CardItem>
                        </Card>
                      )  
                });

             return (
              <Content style={styles.st_header}>
                  <Title style={styles.st_title1}> Input Data </Title>
                  <Title style={styles.st_nama}> {this.state.username} </Title>
                  <View style={styles.footerBottomSignUp}>
                  <TouchableOpacity onPress={() => navigate('Beranda')}>
                    <Text style={styles.st_signup}>
                      Kembali ke beranda {'\n'}
                    </Text>
                  </TouchableOpacity>
                  </View>
                  {dataSurvey}
              </Content>
            );

       }else{
           let dataSurvey = this.state.data.map((data, index) => {
                    return (
                          <Card key={index} style={styles.st_card1}>
                              <CardItem>
                              <Left>
                                <Icon active name="medkit" />
                                <Body>
                                  <Text style={styles.right}>{data.tanggal}</Text>
                                  <Text style={styles.right}>{data.jam}</Text>
                                </Body>
                              </Left>
                              <Right>
                                <Icon name="arrow-forward" onPress={() => navigate('DetailDataPasien', {id_survey : data.id_survey})}/>
                              </Right>
                            </CardItem>
                        </Card>
                      )  
                });


             return (
              <Content style={styles.st_header}>
                  <Title style={styles.st_title1}> Input Data </Title>
                  <Title style={styles.st_nama}> {this.state.username}</Title>
                  <View style={styles.footerBottomSignUp}>
                  <TouchableOpacity onPress={() => navigate('Beranda')}>
                    <Text style={styles.st_signup}>
                      Kembali ke beranda {'\n'}
                    </Text>
                  </TouchableOpacity>
                  </View>
                  {dataSurvey}
              </Content>
            );

       }
           

                

        
    }


  render() {
     const { navigate } = this.props.navigation;
     if (this.state.data.length > 0) {
           return (
              <Container style={styles.container}>
              <Loader loading={this.state.loading} />
              <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                 {this._renderSurvey()}
              </Container>
            );
     }else{
             return (
                <Container style={styles.container}>
                    <Loader loading={this.state.loading} />
                    <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                     <Content style={styles.st_header}>
                        <Title style={styles.st_title1}> Input Data </Title>
                        <Title style={styles.st_nama}> {this.state.username} </Title>
                        <View style={styles.footerBottomSignUp}>
                  <TouchableOpacity onPress={() => navigate('Beranda')}>
                    <Text style={styles.st_signup}>
                      Kembali ke beranda {'\n'}
                    </Text>
                  </TouchableOpacity>
                  </View>
                 <Text style={{justifyContent:'center', alignItems: 'center', textAlign: 'center', color:'white',fontSize: 20,}}> {'\n'}{'\n'}DATA BELUM ADA</Text>
                    </Content>
                </Container>
              );
     }
 
  }
}

const styles = StyleSheet.create({
	container:{
	   flex: 1,
	},
	bgimage:{
      flex: 1,
//      backgroundColor: '#16a085',
      resizeMode: 'cover',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    st_header:{
    	marginTop: 20,
    },
    st_nama :{
      color:'white',
      marginBottom:20,
    },
    st_size:{
    	fontSize: 30,
    	color: 'white',
    },
    st_title1:{
      color:'white',
      alignItems: 'center',
      fontSize: 20,
      fontWeight: '400',
      marginTop:20,
    },
    st_title3:{
      color: '#ffffff', 
      fontSize: 17,
      marginLeft: 40,
      marginRight: 40,
    },
    st_card1:{
      marginTop: 0,
      marginLeft: 15,
      marginRight: 15,
    },
    st_signup:{
      color: 'white',
      fontWeight: '500', 
      textAlign: 'center',
    },
    right : {
      width:200,
    }
});

export default inputData;