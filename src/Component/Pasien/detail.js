'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Alert
} from 'react-native';

import { Container,
		     Content, 
         Header,
         Body,
         Text,
         Button,
         Icon,
         Title,
         Card,
         Left,
         CardItem,
         Right
       } from 'native-base';

import { Col, Row, Grid } from 'react-native-easy-grid';

import Modal from 'react-native-modal';

import Loader from '../Indicator/loader';

class detailData extends Component {
    constructor(props) {
    super(props);
    this.state = {
      data : [],
      dataRek : [],
      valid : '',
      token : '',
      skor : 0,
      derajat : '',
      idDerajat : 0,
      isModalVisible : false,
      loading : true,
    }
  }

     componentDidMount() {
        this.getAllData();
    }


    getAllData(){
        AsyncStorage.getItem('tokenUser', (error, result1) => {
            if (result1) {

                            const {state} = this.props.navigation;
                             this.setState({ 
                                        valid : state.params.valid,
                                        token : result1
                                  });
                              fetch('http://dev.infinite-creative.com/sispak_api/Survey/skorkriteria/' + state.params.id_survey, {
                               method: 'GET',
                               headers: {
                                            'Content-Type': 'application/json',
                                            'Accept': 'application/json',
                                            'token': result1
                                          }
                                })
                                .then((response) => response.json())
                                .then((response) => {
                                  
                                  var jum = 0;
                                  for(var i = 0 ; i < response.data.length ; i++){
                                    jum += parseInt(response.data[i].nilai);
                                  }
                                   this.setState({ 
                                      skor : jum,
                                    });
                                  this.setState({ 
                                      data : response.data,
                                    });

                                  this.setState({ 
                                      loading : false,
                                    });


                                   fetch('http://dev.infinite-creative.com/sispak_api/Survey/skorderajat/' + state.params.id_survey, {
                                         method: 'GET',
                                         headers: {
                                                      'Content-Type': 'application/json',
                                                      'Accept': 'application/json',
                                                      'token': result1
                                                    }
                                    })
                                    .then((response) => response.json())
                                    .then((response) => {
                                        console.log(response);
                                          this.setState({ 
                                              derajat : response.derajat.keterangan,
                                            });

                                          this.setState({ 
                                              idDerajat : response.derajat.id,
                                            });


                                          //data rekom
                                          fetch('http://dev.infinite-creative.com/sispak_api/Survey/rekomendasi/' + response.derajat.id, {
                                               method: 'GET',
                                               headers: {
                                                            'Content-Type': 'application/json',
                                                            'Accept': 'application/json',
                                                            'token': result1
                                                          }
                                          })
                                          .then((response) => response.json())
                                          .then((response) => {
                                               
                                                 this.setState({ 
                                                    dataRek : response.data,
                                                  });
                                                
                                          
                            
                                          }).done();



                                    
                      
                                    }).done();



                      
                                }).done();
            }
        });
  
    }
      _showModal(){
          this.setState({ isModalVisible: true });
      }

      _hideModal() {
          this.setState({ isModalVisible: false });
      }

      validData(mode) {
        const {state} = this.props.navigation;
        console.log(state.params.id_survey);

                fetch('http://dev.infinite-creative.com/sispak_api/Survey/setvalid', {
                     method: 'POST',
                     headers: {
                                  'Content-Type': 'application/json',
                                  'Accept': 'application/json',
                                  'token' : this.state.token
                                },
                      body: JSON.stringify({
                          id_survey : state.params.id_survey,
                          valid: mode,
                       })
                  })
                .then((response) => response.json())
                .then((response) => {
                      console.log(response);

                       if (response.status == 'success') {
                          Alert.alert('Pesan','Perubahan berhasil dilakukan');
                          this.setState({ 
                            valid : mode,
                            
                      });
                       }
  
    
  
                }).done();

      }



       _renderSurvey() {
       const { navigate } = this.props.navigation;
        let skor = this.state.skor;
        let drjt = this.state.derajat;
        const {state} = this.props.navigation;
            let dataSurvey = this.state.data.map((data, index) => {
                    if (index == 0) {
                         return (
                                 <Card key={index} style={styles.st_card2}>
                                    <CardItem>
                                      <Left>
                                        <Body>
                                          <Text >{data.nama_kriteria}</Text>
                                          <Text note>Skor : {data.nilai}</Text>
                                        </Body>
                                      </Left>
                                    </CardItem>
                              </Card>
                              );
                    }else{
                      return(
                           <Card key={index} style={styles.st_card2chld}>
                                      <CardItem>
                                        <Left>
                                        <Body>
                                             <Text >{data.nama_kriteria}</Text>
                                              <Text note>Skor : {data.nilai}</Text>
                                        </Body>
                                        </Left>
                                      </CardItem>
                                </Card>
                      );
                    }
                });

          let dataRekom = this.state.dataRek.map((data, index) => {
                return (
                               <Card key={index}  style={styles.st_cardMdl}>
                                <CardItem style={styles.st_carditem1}>
                                  <Left>
                                       <Text style={styles.st_txtrekom}>{data}</Text>
                                  </Left>
                                </CardItem>
                            </Card>
                );
          });

    
            if (this.state.valid == 'n') {
                 return (
              <Content style={styles.st_header}>
                    <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                    <Title style={styles.st_nama}> Pasien </Title>
                    <Card style={styles.st_score}>
                        <Text style={styles.st_txtskor}>Skor</Text>
                        <Text style={styles.st_txtangka}>{skor}</Text>
                    </Card>

                    <Card style={styles.st_card1}>
                          <CardItem style={styles.st_carditem1}>
                            <Left>
                                <Text style={styles.st_txtcard}>{drjt}</Text>
                            </Left>
                          </CardItem>
                    </Card>
                    {dataSurvey}
            

                    <Grid style={{marginTop:20, marginBottom: 20, alignSelf:'center',marginLeft:20}}>  
                      <Col style={{marginLeft: 50}}>
                        <Button success style={{width:125}} onPress={() => navigate('DetailDataPasien', {id_survey : state.params.id_survey})}>
                          <Text style={{marginLeft: 25}}>Detail</Text>
                        </Button> 
                      </Col>
                      <Col style={{marginRight: 50}}>
                        <Button success onPress={() => this._showModal()}>
                          <Text>Rekomendasi</Text>
                        </Button>
                      </Col>
                    </Grid>

                    <Grid style={{marginTop:20, marginBottom: 20}}>  
                      <Col style={{alignItems: 'center'}}>

                           <Text>Not Valid</Text>
                            <Button style={{marginTop:10, alignSelf:'center'}} info onPress={() => this.validData('y')}>
                              <Text>Validasi</Text>
                            </Button>

                      </Col>
                    </Grid>
            
                    <View style={styles.footerBottom}>
                            <TouchableOpacity onPress={() => navigate('Beranda')}>
                              <Text style={styles.st_signup}>
                                Kembali ke beranda {'\n'}
                              </Text>
                            </TouchableOpacity>
                  </View>

                    <Modal isVisible={this.state.isModalVisible}>
                        <View style={styles.st_modalView}>
                            <Content>
                            {dataRekom}

                            <Button success block onPress={() => this._hideModal()}>
                              <Text>OK</Text>
                            </Button>
                            </Content>
                            
                        </View>
                    </Modal>
 
              </Content>

            );
            }else{
               return (
                      <Content style={styles.st_header}>
                            <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                            <Title style={styles.st_nama}> Pasien </Title>
                            <Card style={styles.st_score}>
                                <Text style={styles.st_txtskor}>Skor</Text>
                                <Text style={styles.st_txtangka}>{skor}</Text>
                            </Card>

                            <Card style={styles.st_card1}>
                                  <CardItem style={styles.st_carditem1}>
                                    <Left>
                                        <Text style={styles.st_txtcard}>{drjt}</Text>
                                    </Left>
                                  </CardItem>
                            </Card>
                            {dataSurvey}
                    

                            <Grid style={{marginTop:20, marginBottom: 20}}>  
                              <Col style={{marginLeft: 50}}>
                                <Button success style={{width:125}} onPress={() => navigate('DetailDataPasien', {id_survey : state.params.id_survey})}>
                                  <Text style={{marginLeft: 25}}>Detail</Text>
                                </Button> 
                              </Col>
                              <Col style={{marginRight: 50}}>
                                <Button success onPress={() => this._showModal()}>
                                  <Text>Rekomendasi</Text>
                                </Button>
                              </Col>
                            </Grid>

                            <Grid style={{marginTop:20, marginBottom: 20}}>  
                              <Col style={{alignItems: 'center'}}>
                                
                                  <Text>Valid</Text>
                                <Button style={{marginLeft:'35%',marginTop:10}} info onPress={() => this.validData('n')}>
                                  <Text>Batal Validasi</Text>
                                </Button>

                              </Col>
                            </Grid>
                    
                            <View style={styles.footerBottom}>
                                    <TouchableOpacity onPress={() => navigate('Beranda')}>
                                      <Text style={styles.st_signup}>
                                        Kembali ke beranda {'\n'}
                                      </Text>
                                    </TouchableOpacity>
                          </View>

                            <Modal isVisible={this.state.isModalVisible}>
                                <View style={styles.st_modalView}>
                                    <Content>
                                    {dataRekom}

                                    <Button success block onPress={() => this._hideModal()}>
                                      <Text>OK</Text>
                                    </Button>
                                    </Content>
                                    
                                </View>
                            </Modal>
         
                      </Content>

                    );
            }
          
    }


  render() {
     const { navigate } = this.props.navigation;
    return (
    	<Container>
      <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
	    	  {this._renderSurvey()}
    	</Container>
    );
  }
}

const styles = StyleSheet.create({
    st_header:{
    	flex: 1,
//        backgroundColor: '#16a085',
    },
    st_nama :{
      fontSize: 30,
      marginTop:20,
      marginBottom:10,
      alignItems: 'center',
      alignSelf: 'center',
      marginLeft:-30,
    },
    st_size:{
    	fontSize: 30,
    	color: 'white',
    },
    st_title1:{
      alignItems: 'center',
      fontSize: 20,
      fontWeight: '400',
      marginTop:20,
    },
    st_title3:{
      color: '#ffffff', 
      fontSize: 17,
      marginLeft: 40,
      marginRight: 40,
    },
    st_txtskor:{
      color: 'white',
      marginTop: 40,
      marginLeft: 55,
      fontSize: 20, 
      width:50,
    },
     st_txtangka:{
      color:'white',
      marginLeft: 45,
      fontSize: 26, 
      width:100,
    },
    st_carditem1 : {
       alignItems: 'center',
      backgroundColor :'#f0ad4e' 
    },
    st_card1:{
      marginTop: 0,
      alignSelf:'center',
      width: 300,
     
    },
    st_card2:{
      marginTop:20,
      marginLeft:15,
      marginRight: 15,
      alignItems: 'center',
    },
    st_txtrekom :{
       color: '#ffffff',
    },
    st_card2chld :{
      marginLeft:15,
      marginRight: 15,
      marginTop: -1,
      alignItems: 'center',
    },
    footerBottom : {
      alignItems: 'center',
    },
    st_txtcard : {
      textAlign: 'center', 
    },
    st_score:{
      flex: 0,
      borderRadius: 150,
      alignSelf:'center',
      width:150,
      height: 150,
      backgroundColor: '#f0ad4e',
      marginBottom: 20
    },
    st_modalView : {
       alignItems: 'center', 
       flex : 1,
    },
    st_cardMdl : {
        marginTop: 30,
        marginLeft:15,
        marginRight: 15,
        width: 300,
    },
    right : {
      width:200,
    },
    bgimage:{
      flex: 1,
      resizeMode: 'cover',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      width: '100%',
      height: '100%',
      },
});

export default detailData
