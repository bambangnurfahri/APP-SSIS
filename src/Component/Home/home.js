'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  AppRegistry,
  AsyncStorage,
  Image,
  ScrollView,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Container, 
		 Header, 
		 Content, 
		 Thumbnail, 
		 Text,
		 Button
} from 'native-base';
export default class home extends Component {


  _checkLogin = (navigate) => {
    AsyncStorage.getItem('tokenUser', (error, result) => {
      if (result) {
         AsyncStorage.getItem('statusUser', (error, resultz) => {
            if (resultz == 'dokter') {
                 navigate('Beranda',{status : 'Pasien'});
            }else{
                 navigate('Beranda',{status : 'Data'});
            }
          });
        
      }else{
        navigate('Login');
      }
    });
  }

  render() {
  	const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <View style={styles.container}>
        <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
      	<Text>{'\n'}</Text>
      	<Thumbnail square large source={require('../../Images/logo.png')}/>
        <Text>{"\n"}</Text>
      	<Text style={styles.fonthome}>Spine Surgery Injury Severity (SSIS) Score atau Skor Prediksi Keselamatan Pasien pada operasi Tulang Belakang (PKPOTB) terdiri dari 5 Kriteria (Kehidupan, 
          Saraf, Tulang , Otot dan Kulit). Kelima Kriteria tersebut diuraikan menjadi 32 Paramater (berupa pertanyaan).{'\n'}{'\n'}Saat mengisi SSIS Score anda akan menemukan
          beberapa parameter yang muncul berulang. Parameter yang sama tersebut nilainya berbeda tergantung masuk dalam kriteria yang mana parameter tersebut.
          {'\n'}{'\n'}Pasien hanya dapat mengisi parameter dan tidak mengetahui hasil/Skornya.Untuk mengetahui hasil pengisian parameter, pasien harus menghubungi
          dokter yang bersangkutan.{'\n'}{'\n'}SSIS Score di prakarsai oleh DR.dr. Rahyussalim, Sp.OT(K)-Spine.{'\n'}{'\n'}SSIS Score Developed by Incodeonline.com
        </Text>
      	<Content>
      		<Text>{'\n'}</Text>
      		<Button block info style={styles.st_button} onPress={() => this._checkLogin(navigate)}>
          <Text style={styles.st_text}>MULAI</Text>
          </Button>
      	</Content>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
//      backgroundColor: '#16a085',
    },
    fonthome:{
    	fontSize: 17,
    	color: 'white',
      backgroundColor: 'transparent',
      paddingLeft: '10%',
      paddingRight:'10%',
      justifyContent:'center',
      alignItems:'center',
      textAlign: 'justify',
    },
    bgimage:{
      flex: 1,
      resizeMode: 'cover',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    st_button:{
      width: 200,
      height: 50,
      borderRadius: 20,
    },
    st_text:{
      fontSize: 18,
    }
    });

AppRegistry.registerComponent('home', () => home );