'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  Platform,
  PixelRatio,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from 'react-native';

import { Container,
     Content, 
         Header,
         Text,
         Button,
         Icon,
         Title,
         Form,
         Body,
         Card,
         Item,
         Picker,
         Label,
         Input, 
         List,
         ListItem
       } from 'native-base';


import { Col, Row, Grid } from 'react-native-easy-grid';

import DatePicker from 'react-native-datepicker';

import ImagePicker from 'react-native-image-picker';

var FileUpload = require('NativeModules').FileUpload;

class profil extends Component {
constructor() {
    super();
    this.state = {
      status : '',
      idUser : '',
      data: [],
      nama : '',
      username : '',
      password : '',
      email : '',
      dokter :[],
      rs:[],
      avatarSource: null,
      imgBase64: '',
      dataPhoto : [],
    }
  }


     selectPhotoTapped() {
                const options = {
                  quality: 1.0,
                  maxWidth: 500,
                  maxHeight: 500,
                  storageOptions: {
                    skipBackup: true
                  }
                };

                ImagePicker.showImagePicker(options, (response) => {
                  console.log('Response = ', response);

                  if (response.didCancel) {
                    console.log('User cancelled photo picker');
                  }
                  else if (response.error) {
                    console.log('ImagePicker Error: ', response.error);
                  }
                  else if (response.customButton) {
                    console.log('User tapped custom button: ', response.customButton);
                  }
                  else {
                    var source, temp;
                    // You can display the image using either:
                    //source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

                    temp = response.data;

                    //Or:
                    if (Platform.OS === 'android') {
                      source = {uri: response.uri, isStatic: true};
                    } else {
                      source = {uri: response.uri.replace('file://', ''), isStatic: true};
                    }

                    this.setState({
                      avatarSource: source,
                      imgBase64: temp,
                      dataPhoto : response,
                    });
                  }
                });
  }

     getDataDokter() {
       fetch('http://dev.infinite-creative.com/sispak_api/Dokter/', {
       method: 'GET',
       headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    // 'Authorization': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VybmFtZSI6ImFkbWluIn0.F5-b8XNvMzFcE7uyYTQwX_HaQBpADkO1epAdiqJ45EI'
                  }
        })
        .then((response) => response.json())
        .then((response) => {
               this.setState({dokter : response.data});
        }).done();
  }


    getDataRumahSakit() {
       fetch('http://dev.infinite-creative.com/sispak_api/Rumahsakit/', {
       method: 'GET',
       headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                  }
        })
        .then((response) => response.json())
        .then((response) => {
               this.setState({rs : response.data});
        }).done();
  }


  getData(){
        AsyncStorage.getItem('tokenUser', (error, result1) => {
            if (result1) {
              var source;
              var url_photo = "http://dev.infinite-creative.com/sispak_api/upload/";

                     AsyncStorage.getItem('statusUser', (error, result2) => {
                        if (result2) {
                              
                              AsyncStorage.getItem('idUser', (error, result) => {
                                        this.setState({
                                                  idUser : result,
                                                });
                                    if (result) {
                                        if (result2 == 'dokter') {
                                          fetch('http://dev.infinite-creative.com/sispak_api/Dokter/' + result, {
                                           method: 'GET',
                                           headers: {
                                                        'Content-Type': 'application/json',
                                                        'Accept': 'application/json'
                                                        // 'token': result1
                                                      }
                                            })
                                            .then((response) => response.json())
                                            .then((response) => {
                                              
                                              if (response.data[0].foto == "") {
                                                source = null;
                                              }else{
                                                source = {uri: url_photo+response.data[0].foto, isStatic: true};
                                              }
                                              
                                             
                                              this.setState({
                                                  data : response.data[0],
                                                  status : result2,
                                                  avatarSource : source
                                                });
                                                    console.log (this.state);
                                              this.setState({
                                                  selectedRS: response.data[0].id_rumahsakit
                                                });

                                            }).done();
                                        }else{
                                          fetch('http://dev.infinite-creative.com/sispak_api/Pasien/' + result, {
                                           method: 'GET',
                                           headers: {
                                                        'Content-Type': 'application/json',
                                                        'Accept': 'application/json'
                                                        // 'token': result1
                                                      }
                                            })
                                            .then((response) => response.json())
                                            .then((response) => {

                                              if (response.data[0].foto == "") {
                                                source = null;
                                              }else{
                                                source = {uri: url_photo+response.data[0].foto, isStatic: true};
                                              }
                                             
                                              this.setState({
                                                  data : response.data[0],
                                                  status : result2,
                                                  avatarSource : source
                                                });

                                      
                                               this.setState({
                                                  selectedDokter: response.data[0].id_dokter,
                                                  selectedGender : response.data[0].gender,
                                                  date : response.data[0].tanggal_lahir
                                                });

                                            }).done();
                                        }
                                    }
                                });




                        }
                    });


                      



            }
        });

    }

  componentDidMount() {
      this.getData();
      this.getDataDokter();
      this.getDataRumahSakit();
  }

    onValueGender(value: string) {
    this.setState({
      selectedGender: value
    });
  }

  handleUpdate() {
        if (this.state.nama == ""){
            this.state.nama = this.state.data.nama;
        }

        if (this.state.username == "") {
            this.state.username = this.state.data.username;
        }

        if (this.state.email == "") {
            this.state.email = this.state.data.email;
        }


       
      
        AsyncStorage.getItem('tokenUser', (error, result1) => {
                if (this.state.status == 'pasien') {

                   if (this.state.imgBase64 != '') {
                         var obj = {
                          uploadUrl: 'http://dev.infinite-creative.com/sispak_api/Pasien/upload/',
                          method: 'POST', // default 'POST',support 'POST' and 'PUT'
                          headers: {
                            'Accept': 'application/json',
                          },
                          fields: {
                            'img': this.state.imgBase64,
                            'id' : this.state.idUser
                          },
                          files: [{
                              name: 'Fileku', // optional, if none then `filename` is used instead
                              filename: this.state.dataPhoto.fileName, // require, file name
                              filepath: this.state.dataPhoto.path, // require, file absoluete path
                              filetype: this.state.dataPhoto.type, // options, if none, will get mimetype from `filepath` extension
                          }]

                      };
                      FileUpload.upload(obj, function(err, result) {
      
                        console.log('upload:', err, result);
                        if (err == null){
                         console.log('berhasil masuk');
                        }else{
                         console.log('error : ' + err);
                        }

                      })
                    }

              fetch('http://dev.infinite-creative.com/sispak_api/Pasien/update', {
                 method: 'POST',
                 headers: {
                              'Accept': 'application/json',
                              'Content-Type': 'application/json',
                              'token': result1
                            },
                  // body : 
                  body: JSON.stringify({
                        id_pasien : this.state.idUser,
                        nama: this.state.nama,
                        username: this.state.username,
                        email: this.state.email,
                        gender : this.state.selectedGender,
                        id_dokter : this.state.selectedDokter,
                        tanggal_lahir : this.state.date,
                       })
                  })
                  .then((response) => response.json())
                  .then((response) => {

                      console.log(response);
                          
                           if (response.status != 'fail') {
                             Alert.alert("Pesan","Berhasil update profile pasien");
                          }else{
                            Alert.alert("Pesan",response.pesan);
                          }
                         // this.setState({success : response.success});

                  }).done();
          }else{
            
                console.log(this.state.imgBase64);
                  if (this.state.imgBase64 != '') {
                         var obj = {
                          uploadUrl: 'http://dev.infinite-creative.com/sispak_api/Dokter/upload/',
                          method: 'POST', // default 'POST',support 'POST' and 'PUT'
                          headers: {
                            'Accept': 'application/json',
                          },
                          fields: {
                            'img': this.state.imgBase64,
                            'id' : this.state.idUser
                          },
                           files: [{
                              name: 'Fileku', // optional, if none then `filename` is used instead
                              filename: this.state.dataPhoto.fileName, // require, file name
                              filepath: this.state.dataPhoto.path, // require, file absoluete path
                              filetype: this.state.dataPhoto.type, // options, if none, will get mimetype from `filepath` extension
                          }]

                      };
                      FileUpload.upload(obj, function(err, result) {
                        console.log('upload:', err, result);
                        if (err == null){
                         console.log('berhasil masuk');
                        }else{
                         console.log('error : ' + err);
                        }

                      })
                    }

                fetch('http://dev.infinite-creative.com/sispak_api/Dokter/update', {
                 method: 'POST',
                 headers: {
                              'Accept': 'application/json',
                              'Content-Type': 'application/json',
                              'token': result1
                            },
                  // body : 
                  body: JSON.stringify({
                        id_dokter : this.state.idUser,
                        nama: this.state.nama,
                        username: this.state.username,
                        email: this.state.email,
                        id_rumahsakit : this.state.selectedRS,
                       })
                  })
                  .then((response) => response.json())
                  .then((response) => {
                          
                   
                          if (response.status!= 'fail') {
                             Alert.alert("Pesan","Berhasil Update data dokter");
                          }else{
                            Alert.alert("Pesan",response.pesan);
                          }
                         this.setState({success : response.success});

                  }).done();
          }
        });
    
  }

  onValueDokter(value: string) {
    this.setState({
      selectedDokter: value
    });
  }

  onValueRS(value: string) {
    this.setState({
      selectedRS: value
    });
  }

  render() {
              if (this.state.status == 'dokter') {
                    let dataRs = [
                          {nama: 'Pilih Rumah sakit', value: 'key0'}
                      ];
                    if (this.state.rs.length > 0) {
                        this.state.rs.forEach((data) => dataRs.push({nama: data.nama, id_rumahsakit: data.id_rumahsakit}));
                    }
                      let comboRumahSakit = dataRs.map((data, index) =>{
                        return (
                             <Item key={index} label={data.nama} value={data.id_rumahsakit} />
                      )
                  
                  });
                        return (
                                 // <Container style={styles.container}>
                                
                                  <Grid style={{flex:1}}>
                                    <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                                    <Row>
                                       <Col>
                                          <Content style={{width: '100%'}}>
                                              <Button transparent style={styles.st_header} onPress={() => this.props.navigation.navigate('DrawerOpen')}>
                                                <Icon ios='ios-menu' android="md-menu" style={styles.st_size} />
                                              </Button>
                                                <Title style={styles.st_title1}> PROFIL DOKTER </Title>

                                                <TouchableOpacity style={styles.avatarContainer} onPress={this.selectPhotoTapped.bind(this)}>
                                                    <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                                                    { this.state.avatarSource === null ? <Text>Select a Photo</Text> :
                                                      <Image style={styles.avatar} source={this.state.avatarSource} />
                                                    }
                                                    </View>
                                                </TouchableOpacity>

                                                <Form style={{paddingRight: 20}}>
                                                <Item stackedLabel>
                                                  <Label><Text style={styles.st_inputfnt}>Nama</Text></Label>
                                                  <Input style={styles.st_inputfnt} onChangeText={(text) => this.setState({nama:text})} defaultValue={this.state.data.nama}/>
                                                </Item>
                                                 <Item stackedLabel>
                                                   <Label><Text style={styles.st_inputfnt}>Username</Text></Label>
                                                  <Input style={styles.st_inputfnt} onChangeText={(text) => this.setState({username:text})}  defaultValue={this.state.data.username}/>
                                                </Item>
                                                 <Item stackedLabel>
                                                  <Label><Text style={styles.st_inputfnt}>Email</Text></Label>
                                                  <Input style={styles.st_inputfnt} onChangeText={(text) => this.setState({email:text})} defaultValue={this.state.data.email}/>
                                                </Item>
                                             
                                                  
                                                  <Picker style={styles.st_rs} mode="dropdown" selectedValue={this.state.selectedRS} onValueChange={this.onValueRS.bind(this)}>
                                                      {comboRumahSakit}
                                                  </Picker>
                                                </Form>
          
                                                <Button block info style={styles.st_buttonEdit} onPress={() => this.handleUpdate()}>
                                                  <Text style={styles.st_title4}>Simpan</Text>
                                                </Button>
                                        </Content>
                                    
                                       </Col>
                                    </Row>
                                  </Grid>
                            	    	

                                  // </Container>
                        );
            }else{
                      let dataDokter = [
                        {nama: 'Pilih Dokter', value: 'key0'}
                    ];
                  if (this.state.dokter.length > 0) {
                      this.state.dokter.forEach((data) => dataDokter.push({nama: data.nama, id_dokter: data.id_dokter}));
                  
                  }  
                     let comboDokter = dataDokter.map((data, index) =>{
                        return (
                             <Item key={index} label={data.nama} value={data.id_dokter} />
                      )
                  
                  });
              return (
                                  <Grid style={{flex:1}}>
                                    <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                                    <Row>
                                       <Col>
                                          <Content style={{width: '100%'}}>
                                              <Button transparent style={styles.st_header} onPress={() => this.props.navigation.navigate('DrawerOpen')}>
                                                <Icon ios='ios-menu' android="md-menu" style={styles.st_size} />
                                              </Button>
                                                <Title style={styles.st_title1}> PROFIL PASIEN </Title>

                                                 <TouchableOpacity style={styles.avatarContainer} onPress={this.selectPhotoTapped.bind(this)}>
                                                    <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                                                    { this.state.avatarSource === null ? <Text>Select a Photo</Text> :
                                                      <Image style={styles.avatar} source={this.state.avatarSource} />
                                                    }
                                                    </View>
                                                </TouchableOpacity>

                                                <Form style={{paddingRight: 20}}>
                                    
                                                <Item stackedLabel>
                                                  <Label><Text style={styles.st_inputfnt}>Nama</Text></Label>
                                                  <Input style={styles.st_inputfnt} onChangeText={(text) => this.setState({nama:text})} defaultValue={this.state.data.nama}/>
                                                </Item>
                                                 <Item stackedLabel>
                                                   <Label><Text style={styles.st_inputfnt}>Username</Text></Label>
                                                  <Input style={styles.st_inputfnt} onChangeText={(text) => this.setState({username:text})}  defaultValue={this.state.data.username}/>
                                                </Item>
                                                 <Item stackedLabel>
                                                  <Label><Text style={styles.st_inputfnt}>Email</Text></Label>
                                                  <Input style={styles.st_inputfnt} onChangeText={(text) => this.setState({email:text})} defaultValue={this.state.data.email}/>
                                                </Item>
                                                      <Picker style={styles.st_rs} mode="dropdown" selectedValue={this.state.selectedGender} onValueChange={this.onValueGender.bind(this)}>
                                                          <Item label="Jenis Kelamin" value="key0" />
                                                          <Item label="Laki-Laki" value="l" />
                                                          <Item label="Wanita" value="p" />
                                                      </Picker>
                                                  
                                                  <Picker style={styles.st_rs}mode="dropdown" selectedValue={this.state.selectedDokter} onValueChange={this.onValueDokter.bind(this)}>
                                                    {comboDokter}
                                                  </Picker>
                                                </Form>

                                                  <DatePicker
                                                    style={styles.st_datepck}
                                                    date={this.state.date}
                                                    mode="date"
                                                    placeholder="Tanggal Lahir"
                                                    format="YYYY-MM-DD"
                                                    confirmBtnText="Confirm"
                                                    cancelBtnText="Cancel"
                                                    onDateChange={(date) => {this.setState({date: date})}}/>

                                                <Button block info style={styles.st_buttonEdit} onPress={() => this.handleUpdate()}>
                                                  <Text style={styles.st_title4}>Simpan</Text>
                                                </Button>
                                        </Content>
                                    
                                       </Col>
                                    </Row>
                                  </Grid>

            



              );
                     
            }
    }


 }

const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'center',
      alignItems: 'center',
  },
  bgimage:{
      flex: 1,
//      backgroundColor: '#16a085',
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    st_header:{
      marginTop: 15,
    },
    st_size:{
      fontSize: 30,
      color: 'white',
    },
    st_title1:{
      alignItems: 'center',
      fontSize: 20,
      marginBottom: 20,
      fontWeight: '400', 
      color:'white',
    },
    st_title3:{
      color: '#ffffff', 
      fontSize: 17,
      marginLeft: 65,
      marginRight: 35,
      marginTop: 25,
    },
     st_rs: {
      marginHorizontal: -2,
      marginLeft: 10,
      width: '100%',
      color: 'white',
  },
    st_form:{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',  
    },
    st_card:{
      backgroundColor: '#FFFFFF50',
      marginRight: 18,
    },
    st_button:{
      width: 300,
      height: 50,
      borderRadius: 20, 
      marginTop: 30,
      marginLeft: 55,
      marginRight: 55,
    },
    st_buttonEdit:{
      width: 200,
      height: 50,
      borderRadius: 20, 
      alignItems: 'center',
      justifyContent: 'center',
      alignSelf: 'center',
      marginTop: 20,
   
    },
    st_title4:{
      fontSize: 15,
      color: '#ffffff',
      paddingLeft: 20,
      justifyContent: 'center',
      alignItems: 'center',
    },
    st_item:{
      marginRight: 20,
      marginLeft:20,
      marginTop: 5,
      backgroundColor: '#FFFFFF50',

    },
        st_datepck:{
      marginHorizontal: 14,
      width: '96%',
      borderColor: '#ffffff',
      borderWidth: 1,
  },
    st_text:{
      color: '#ffffff',
      paddingLeft: 10,
    },
    st_inputfnt:{
      color: 'white',
      marginRight:7,
    },
  avatar: {
    borderRadius: 75,
    width: 150,
    height: 150
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderTopWidth: 0,
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default profil;
// GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;
<script src="http://localhost:8097"></script>