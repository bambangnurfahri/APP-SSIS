'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  AppRegistry,
  Image,
  ScrollView,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Container, 
     Header, 
     Content, 
     Thumbnail, 
     Text,
     Button,
     Icon
} from 'native-base';
export default class tentang extends Component {
  static navigationOptions = {
    title: 'Tentang SSIS Score',
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
      <View style={styles.container}>

      <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
      <Button transparent style={styles.st_header} onPress={() => this.props.navigation.navigate('DrawerOpen')}>
		    		<Icon ios='ios-menu' android="md-menu" style={styles.st_size} />
		    	</Button>
        <Thumbnail square large style={styles.st_logo} source={require('../../Images/logo.png')}/>
        <Text>{'\n'}</Text>
        <Text style={styles.font}>Spine Surgery Injury Severity (SSIS) Score atau Skor Prediksi Keselamatan Pasien pada operasi Tulang Belakang (PKPOTB) terdiri dari 5 Kriteria (Kehidupan, 
          Saraf, Tulang , Otot dan Kulit). Kelima Kriteria tersebut diuraikan menjadi 32 Paramater (berupa pertanyaan).{'\n'}{'\n'}Saat mengisi SSIS Score anda akan menemukan
          beberapa parameter yang muncul berulang. Parameter yang sama tersebut nilainya berbeda tergantung masuk dalam kriteria yang mana parameter tersebut.
          {'\n'}{'\n'}Pasien hanya dapat mengisi parameter dan tidak mengetahui hasil/Skornya.Untuk mengetahui hasil pengisian parameter, pasien harus menghubungi
          dokter yang bersangkutan.{'\n'}{'\n'}SSIS Score di prakarsai oleh DR.dr. Rahyussalim, Sp.OT(K)-Spine.{'\n'}{'\n'}SSIS Score Developed by Incodeonline.com
        </Text>
      </View>
    </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
//      backgroundColor: '#16a085',
    },
    bgimage:{
      flex: 1,
      resizeMode: 'cover',
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    font:{
      fontSize: 20,
    	color: 'white',
      backgroundColor: 'transparent',
      paddingLeft: '12%',
      paddingRight:'12%',
      justifyContent:'center',
      alignItems:'center',
      textAlign: 'justify', 
      
    },
    st_button:{
      width: 200,
      height: 50,
      borderRadius: 20,
    },
    st_header:{
    	marginTop: '4%',
      marginLeft: -1,
    },
    st_text:{
      fontSize: 25,
    },
    st_logo:{
      marginTop: '7%',
      width: 100,
      height: 100,
    },
    st_size:{
      fontSize: 30,
      color: 'white',
    },
    });