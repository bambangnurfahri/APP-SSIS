'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  AsyncStorage,
  TouchableOpacity
} from 'react-native';

import { Container,
		     Content, 
         Header,
         Text,
         Left,
         Thumbnail,
         Card,
         CardItem,
         Button,
         Body,
         Icon,
         Title,
       } from 'native-base';
import Loader from '../Indicator/loader';

class skor extends Component {
  static navigationOptions = {
    title: 'SSIS Score',
  }
    constructor(props) {
        super(props);
        this.state = {
          totalQ : 0,
          now : 1,
          loading : true,
          token : '',
          rekom : [],
          dokter : '',
          rs : '',
          alamat : '',
          tlp:'',
          ganti : 0,
          selesai : 0,
          score : 0,
          data : [],
          avatarSource: null,
          answerData : [],
        }

        // this._renderQuestion = this._renderQuestion.bind(this);
        this.getAllData = this.getAllData.bind(this);
        this.questionAct = this.questionAct.bind(this);
       
    }

    componentDidMount() {
        this.getAllData();
         this.setState({
            ganti: 1
          });
    }

    getAllData(){
      AsyncStorage.getItem('tokenUser', (error, result) => {
            if (result) {
                  fetch('http://dev.infinite-creative.com/sispak_api/survey/pertanyaan', {
                   method: 'GET',
                   headers: {
                                'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                'token': result
                              }
                    })
                    .then((response) => response.json())
                    .then((response) => {
                    	this.state.token = result;
                      var total = 0;
                      var arrayTemp = [];
                          for(var i = 0 ; i < response.data.kriteria.length ; i++) {
                              total += response.data.kriteria[i].parameter.length;
                              for(var j = 0 ; j < response.data.kriteria[i].parameter.length ; j++) {
                                  arrayTemp.push(response.data.kriteria[i].parameter[j]);
                              } 
                          }
                          this.setState({
                            totalQ: total
                          });

                          this.setState({
                            data : arrayTemp
                          });

                          this.setState({
                            loading: false
                          });


                         

                        AsyncStorage.getItem('statusUser', (error, result) => {
                          if (result) {
                            var source;
                            var url_photo = "http://dev.infinite-creative.com/sispak_api/upload/";

                               this.state.status = result; 
                               if (result == 'pasien') {
                                AsyncStorage.getItem('idUser', (error, result) => {
                                  fetch('http://dev.infinite-creative.com/sispak_api/Dokter/pasien/' + result, {
                                   method: 'GET',
                                   headers: {
                                                'Content-Type': 'application/json',
                                                'Accept': 'application/json',
                                                'token': this.state.token
                                              }
                                    })
                                  .then((response) => response.json())
                                  .then((response) => {

                                    if (response.data[0].foto == "") {
                                      source = null;
                                    }else{
                                      source = {uri: url_photo+response.data[0].foto, isStatic: true};
                                    }

                                      console.log(response);
                                         this.setState({
                                            dokter: response.data[0].nama_dokter,
                                            rs : response.data[0].nama_rumahsakit,
                                            alamat : response.data[0].alamat_rumahsakit,
                                            tlp : response.data[0].telepon,
                                            foto : source,
                                            
                                          });

                                  }).done();

                          });

                               }
                          }
                        });

                    }).done();
            }
        });
    }

     questionAct(id) {
        if (this.state.now >= this.state.totalQ) {
           if (this.state.now == this.state.totalQ && this.state.selesai == 0){
              this.state.selesai = 1;
           		this.state.answerData.push({id_grading : id});
           		fetch('http://dev.infinite-creative.com/sispak_api/survey/insert', {
                   method: 'POST',
                   headers: {
                                'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                'token': this.state.token
                              },
                  body: JSON.stringify({
                       	grading : this.state.answerData 
                       })
                    })
                    .then((response) => response.json())
                    .then((response) => {
                        this.state.score = response.nilai;
                        this.state.rekom = response.rekomendasi;
                        this.state.derajat = response.derajat.keterangan
                        this.setState({
      			            	ganti: 1
      			          	});
			          	this.setState({
			            	selesai: 1
			          	});
                    }).done();
           }
        }else{
        	this.setState({
            	ganti: 1
          	});
           this.state.now = this.state.now + 1;
           this.state.answerData.push({id_grading : id});
        }

          
    }

    _renderQuestion() {
      const { navigate } = this.props.navigation;

    	if (this.state.ganti == 1) {
    		  let total = this.state.totalQ;
		      let now = this.state.now;
          let drjt = this.state.derajat;
		      let questionNow = this.state.data[now-1].nama;
          let allrek = this.state.rekom.map((data, index) => {
                    return (
                          <Text key={index} style={styles.st_title4}>{index+1}. {data}{'\n'}</Text>
                      )  
                });
		      let allAnswer = this.state.data[now-1].grading.map((data, index) => {
		                return (
		                		 <Button key={index} block info style={styles.st_button} onPress={() => this.questionAct(data.id)}>
						                <Text style={styles.st_title4}>{data.nama}</Text>
						              </Button>
		                	)  
		            });
		    if (this.state.selesai == 1) {
    			let score = this.state.score;

          if (this.state.status == 'dokter') {
              return(

                
                     <View style={styles.st_answer}>

                        <Card style={styles.st_score}>
                            <Text style={styles.st_txtskor1}>YOUR SCORE</Text>
                            <Text style={styles.st_txtangka}>{score}</Text>

                        </Card>
                           <Content style={{width:300}}>
                          <Card>
                            <CardItem>
                              <Body>
                                <Text style={{fontSize: 14, textAlign:'center'}}>
                                   {drjt}
                                </Text>
                              </Body>
                            </CardItem>
                            <CardItem>
                              <Body>
                                <Text style={styles.st_title4}>
                                   {allrek}
                                </Text>
                              </Body>
                            </CardItem>
                          </Card>
                        </Content>

                        <TouchableOpacity onPress={() => navigate('Beranda')}>
                                      <Text style={styles.st_signup}>
                                        Kembali ke Beranda {'\n'}
                                      </Text>
                                    </TouchableOpacity>
                        
                      </View>


                       
              );
          }else{
            return(
              <View style={{marginTop:10}}>
                    <Title style={styles.st_title1}> Hasil </Title>
                    <Title style={styles.st_txttext3}>Untuk mengetahui</Title>
                    <Title style={styles.st_txttext2}>hasilnya silahkan</Title>
                    <Title style={styles.st_txttext2}>konsultasi kepada</Title>
                    <Title style={styles.st_txttext2}>dokter anda</Title>

                    <View style={styles.card}>
                    <Content>
                          <Card>
                             <CardItem>
                                <Left>
                                  <Thumbnail style={{marginLeft:'20%'}}  source={this.state.foto} />
                                  <Body>
                                    <Text style={{textAlign: 'left'}}>Dokter {this.state.dokter}</Text>
                                  </Body>
                                </Left>
                            </CardItem>
                            <CardItem style={{justifyContent : 'center',marginTop: -10}}>
                                <Left>
                                  <Body>
                                  <Text style={{textAlign: 'center'}}>Rumah Sakit {this.state.rs}</Text>
                                    <Text style={{textAlign: 'center'}}>Alamat {this.state.alamat}</Text>
                                    <Text style={{textAlign:'center'}}>Telp {this.state.tlp}</Text>
                                  </Body>
                                </Left>
                            </CardItem>
                          </Card>
                        </Content>

                         <TouchableOpacity style={styles.st_signup2} onPress={() => navigate('Beranda')}>
                                      <Text >
                                        Kembali ke Beranda {'\n'}
                                      </Text>
                                    </TouchableOpacity>
                    </View>

        
              </View>
               
            );
          }
	    		   
    		}else{

    			return(
		      		 <View style={styles.st_answer}>
                  <Card style={styles.st_scoretny}>
                      <Text style={styles.st_txtskor}>Pertanyaan</Text>
                      <Text style={styles.st_txtangka}>{now}/{total}</Text>
                  </Card>

	                      <Title style={styles.st_titleQ}>{questionNow} ?</Title>
                       {allAnswer}
                       
                       <View style={styles.footerBottomSignUp}>
                       <Text>{'\n'}{'\n'}</Text>
                                    <TouchableOpacity onPress={() => navigate('Beranda')}>
                                      <Text style={styles.st_signup}>
                                        Kembali ke Beranda {'\n'}
                                      </Text>
                                    </TouchableOpacity>
                                    </View>

	             </View>

		      	);
    		}

		      
    	}else{
    	
    		
    	}
    	  
    }


  render() {
    const { navigate } = this.props.navigation;

      if (this.state.data.length > 0) {
            return (
                <Container style={styles.container}>
                    <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                      <Button transparent style={styles.st_header} onPress={() => this.props.navigation.navigate('DrawerOpen')}>
                        <Icon ios='ios-menu' android="md-menu" style={styles.st_size} />
                      </Button>
                   	{this._renderQuestion()}
                </Container>
            );
      }else{
        return <Loader loading={this.state.loading} />;
      }
  }

 
}

const styles = StyleSheet.create({
	container:{
	  flex: 1,
	  justifyContent: 'center',
      alignItems: 'center',
	},
	bgimage:{
      flex: 1,
//      backgroundColor: '#16a085',
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
     st_header:{
      marginTop: 60,
    },
    st_size:{
    	fontSize: 30,
    	color: 'white',
    },
    st_title1:{
      justifyContent: 'center', 
      fontSize: 24,
      fontWeight: '400', 
      fontStyle: 'italic', 
      
    },
    st_title3:{
      color: '#ffffff', 
      fontSize: 25,
      marginLeft: 40,
      marginRight: 40,
    },
     st_title4:{
      fontSize: 14,
    },
    st_titleQ : {
      fontSize : 15,
      marginTop : 40,
    },
    st_answer : {
      flex : 0,
      marginTop:20,
    },
     st_button:{
      width: 300,
      height: 50,
      borderRadius: 20, 
      marginTop: 10,
      marginLeft: 55,
      marginRight: 45,
    },
    st_score:{
      justifyContent: 'center', 
      marginLeft:70,
      flex: 0,
      borderRadius: 150,
      width:150,
      height: 150,
      marginTop : 30,
      backgroundColor: '#f0ad4e',
      marginBottom: 20
    },
    st_scoretny:{
      flex: 0,
      marginLeft: 120,
      borderRadius: 150,
      width:150,
      height: 150,
      backgroundColor: '#f0ad4e',
      marginBottom: 20
    },
    st_txtskor:{
      alignItems: 'center', 
      color: 'white',
      marginTop: 40,
      marginLeft: 20,
      fontSize: 20, 
      width:115,
    },
    st_txtskor1:{
      color: 'white',
      marginTop: 30,
      marginLeft: 25,
      fontSize: 17,
    },
     st_txtangka:{
      color:'white',
      marginLeft: 55,
      fontSize: 17, 
      width:50,
    },
    st_txthasil:{
   
      justifyContent: 'center', 
    },
    footerBottomSignUp:{
      marginTop: 56,
      alignItems: 'center', 
    },
    st_txttext2:{
      fontSize: 20,
      fontStyle: 'italic',
      fontWeight: '600',  
    },
    st_txttext3:{
      marginTop: 20,
      fontSize: 20,
      fontStyle: 'italic',
      fontWeight: '600',  
    },
    st_signup:{
      fontWeight: '500', 
      marginTop: -100,
    },
    st_signup2:{
      marginTop: 10,
      alignItems: 'center',
    },
    card:{
        flex:1,
        marginTop: 20,
        width:300,
    },
});

export default skor;
// GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;