'use strict';

import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Image,
  AsyncStorage
} from 'react-native';

import { Container,
		 Content, 
         Header,
         Text,
         Left,
         Button,
         Icon,
         Body,
         Title,
         Card, 
         CardItem
       } from 'native-base';
import Loader from '../Indicator/loader';

import { NavigationActions } from 'react-navigation';

class beranda extends Component {

   constructor() {
    super();
    this.state = {
      status : '',
      loading: false,
      idUser :0,
    }
  }


   componentDidMount() {


            AsyncStorage.getItem('statusUser', (error, result) => {
            if (result) {
                  this.setState({
                  loading: false,
                });
                this.setState({
                  status: result,
                });
            }
          });

          AsyncStorage.getItem('idUser', (error, result) => {
            if (result) {
                 this.setState({
                  idUser: result,
                });
            }
          });
    }

    _renderButton(){
      const { navigate } = this.props.navigation;
         if (this.state.status == 'dokter') {
            return (
               <Button block info style={styles.st_button} onPress={() => navigate('Data')}>
                <Text style={styles.st_title4}>Pasien</Text>
              </Button>
              );
         }else{
              return (
                 <Button block info style={styles.st_button} onPress={() => navigate('Input', {id : this.state.idUser})}>
                  <Text style={styles.st_title4}>Data</Text>
                </Button>
              );
         }
    }

    _renderHedear(){
        if (this.state.status == 'dokter') {
            return (
                <Title style={styles.st_title2}> DOKTER </Title>
            );
        }else{
            return (
                <Title style={styles.st_title2}> PASIEN </Title>
            );
        }
    }

    _handleLogOut = (navigate) => {
      AsyncStorage.removeItem('tokenUser');
        const resetAction = NavigationActions.reset({
                            index: 0,
                            actions: [
                              NavigationActions.navigate({ routeName: 'Login'})
                            ]
                          })
                        this.props.navigation.dispatch(resetAction);
    }

  render() {
    const { navigate } = this.props.navigation;
    if (this.state.status == '') {
      return (
            <Container style={styles.bgimage}>
              <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                <Content>
                <Loader loading={this.state.loading} />
    
                </Content>
              </Container>
        );
        
    }else{
         return (
              <Container style={styles.container}>
                <Image style={styles.bgimage} source={require('../../Images/Bg.jpg')}/>
                <Content>
                  <Button transparent style={styles.st_header} onPress={() => this.props.navigation.navigate('DrawerOpen')}>
                    <Icon ios='ios-menu' android="md-menu" style={styles.st_size} />
                  </Button>
                    <Title style={styles.st_title1}> MENU </Title>
                      {this._renderHedear()}
                      <Button block info style={styles.st_button} onPress={() => navigate('Skor')}>
                        <Text style={styles.st_title4}>SSIS SCORE</Text>
                      </Button>
                     {this._renderButton()}
                      <Button block info style={styles.st_button} onPress={() => navigate('Profil')}>
                        <Text style={styles.st_title4}>Profil</Text>
                      </Button>
                      <Button block info style={styles.st_button} onPress={() => navigate('Tentang')}>
                        <Text style={styles.st_title4}>Tentang SSIS SCORE</Text>
                      </Button>
                      <Button block info style={styles.st_button} onPress={() => this._handleLogOut(navigate)}>
                        <Text style={styles.st_title4}>Sign Out</Text>
                      </Button>
                </Content>
                       <Title style={styles.st_title3}>Spine Surgery Injury Severity Score</Title>
              </Container>
            );
    }
   
  }
}

const styles = StyleSheet.create({
   container:{
      flex: 1,
    //      backgroundColor: '#16a085',
      justifyContent:'center',
      alignItems: 'center',
    },
    bgimage:{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
    st_header:{
    	marginTop: 15,
      marginLeft: -20,
    },
    st_size:{
    	fontSize: 30,
    	color: 'white',
    },
    st_title1:{
      alignItems: 'center',
      color: '#ffffff',
      fontSize: 20,
      fontWeight: '400', 
    },
    st_title2:{
      alignItems: 'center',
      fontSize: 30,
      color: '#ffffff',
      fontWeight: '800',  
    },
    st_button:{
      width: 300,
      height: 50,
      borderRadius: 20, 
      marginTop: 30,
    },
    st_title3:{
      color: '#ffffff', 
      fontSize: 17,
      textAlign: 'center',
      marginBottom: 70,
    },
    st_title4:{
      fontSize: 20,
      color: '#ffffff',
    }
});


export default beranda;